package com.company;

import java.util.Scanner;

public class PrimeNumber {


    /**
     * citm nr de la tastatura
     * <p>
     * luam toate nr intre 1 și nr citit
     * verificăm pentru fiecare în parte dacă e prim
     * <p>
     * <p>
     * Dați un nr între 1 și 10 000:
     * <p>
     * <p>
     * 100
     * 1 ,2,3,4,5 ..... 100
     * <p>
     * minim = 0
     * maxim = 100
     * <p>
     * <p>
     * <p>
     * <p>
     * 6
     * =========
     * 1
     * 2
     * 3
     * 6
     * <p>
     * în afară de 1 și nr în sine
     * ex pentru 6, divizorii propri sunt 2,3
     * 11 nu are divizori propri
     * <p>
     * 1 - numar
     *
     * @param args
     */


    public static void main(String[] args) {
        System.out.println("Introduceti un nr între 1 și 100 000");
        Scanner keyboard = new Scanner(System.in);

        int numar = keyboard.nextInt();
        int counter = 0;
        if ((numar >= 1) && (numar <= 100000)) {
            System.out.println("Ati introdus un număr corect");
            for (int i = 2; i < numar; i++) {
                if (numar % i == 0) {
                    counter = counter + 1;
                    System.out.println(i);
                }
            }
            System.out.println("Numărul dar are " + counter + " divizori");
        } else {
            System.out.println("Nu ați introdus un nr corect.");
        }
    }
}

package com.company;

import java.util.Scanner;

public class Task11 {

    /**
     * Write an application that will read texts (variables of the String type) until the user gives
     * the text "Enough!" and then writes the longest of the given texts (not including the text
     * "Enough!"). If the user does not provide any text, write "No text provided".
     */

    public static void main(String[] args) {
        System.out.println("Din clasa Task11");
        execute();
    }


    public static void execute() {
        Scanner keyboard = new Scanner(System.in);
        String line = keyboard.nextLine();
        // textul efectiv introdus de utilizator ex: "Ana are mere"
        String longestLine = "";
        // câte caractere are linia ex: 12
        int longest = 0;
        if (line.isEmpty()) {
            System.out.println("No text provided");
            return;
        }
        longest = line.length();
        longestLine = line;
        // pentru primitive comparăm cu ==
        // pentru obiecte comparăm cu metoda .equals()
        while(!line.equals("Enough!")) {
            if(line.length() > longest) {
                longest = line.length();
                longestLine = line;
            }
            line = keyboard.nextLine();
        }
        System.out.println(longestLine);
    }
}

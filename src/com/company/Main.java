package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Din clasa Main");
        Task11.execute();

    }


    /*
     * 1. Fără a implementa funcția de mai jos, calculați, pe foaie, ce va fi afișat în cazul în care input-ul de la tastatură este:
     * a) 12 2 3
     * b) 30 4 5
     * 2. Scrieți două seturi de valori distincte pentru n, x și y pentru care programul va afișa 0 (zero).
     * (aka ce valori trebuie să introducă user-ul ca programul să afișeze 0? Dați două exemple)
     *
     * Ioana was here :D
     * Am facut modificare si din InteliJ :D
     */

    private static void bac() {
        Scanner keyboard = new Scanner(System.in);

        int n, x, y;
        n = keyboard.nextInt();
        x = keyboard.nextInt();
        y = keyboard.nextInt();

        int ok;
        ok = 0;

        for (int i = 1; i <= n; i++) {
            if (((i % x == 0) && (i % y != 0)) || ((i % x != 0) && (i % y == 0))) {
                System.out.print(" " + i);
                ok = 1;
            }
        }

        if (ok == 0) {
            System.out.println(0);
        }
    }

}
